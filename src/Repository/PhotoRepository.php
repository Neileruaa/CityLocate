<?php

namespace App\Repository;

use App\Entity\Photo;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Photo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Photo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Photo[]    findAll()
 * @method Photo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Photo::class);
    }

    public function findAllPhotos(Ville $ville){
        return $this->createQueryBuilder('p')
            ->andWhere('p.ville = :ville')
            ->setParameter('ville', $ville->getId())
            ->getQuery()
            ->getArrayResult()
            ;
    }
}
