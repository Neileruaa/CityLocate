<?php
/**
 * Created by PhpStorm.
 * User: aurelien
 * Date: 10/03/19
 * Time: 14:46
 */

namespace App\Controller;


use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class AdminController
 * @package App\Controller
 * @isGranted("ROLE_USER")
 */
class AdminController extends EasyAdminController {

}