<?php

namespace App\Controller;

use App\Entity\Partie;
use App\Repository\PartieRepository;
use App\Repository\VilleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PartieController extends AbstractController
{
    /**
     * @Route("/partie/new  ", name="Partie.new", methods={"POST"})
     * @param Request $request
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function createNewPartie(Request $request, ObjectManager $manager, TokenGeneratorInterface $tokenGenerator, VilleRepository $villeRepository){
        $parametersAsArray = array();
        if ($content = $request->getContent()){
            $parametersAsArray = json_decode($content, true);
        }

        $token = $tokenGenerator->generateToken();
        $ville = $villeRepository->findOneBy(['name'=>$parametersAsArray['ville']]);
        $newPartie = new Partie();
        $newPartie->setToken($token)
            ->setPseudo($parametersAsArray['pseudo'])
            ->setDifficulte($parametersAsArray['difficulte'])
            ->setScore(0)
            ->setVille($ville)
            ;
        $manager->persist($newPartie);
        $manager->flush();
        return new JsonResponse(['res' => 'success mon bro']);
    }

    /**
     * @Route("/partie/get/all", name="Partie.getAll")
     * @param PartieRepository $partieRepository
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllPartie(PartieRepository $partieRepository, SerializerInterface $serializer, Request $request) {
        $pseudo = $request->get('pseudo');
        $parties = $partieRepository->findAllPartie($pseudo);
        return new JsonResponse($parties);
    }

    /**
     * @Route("/partie/get", name="Partie.currentPartie")
     */
    public function getCurrentPartie(Request $request, PartieRepository $partieRepository){
        $token =  $request->get('token');
        $partie = $partieRepository->findPartieByToken($token);
        return new JsonResponse($partie);
    }
}
