<?php

namespace App\Controller;

use App\Repository\PartieRepository;
use App\Repository\PhotoRepository;
use App\Repository\VilleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PhotoController extends AbstractController
{
    /**
     * @Route("/photo/getAllByPartie", name="Photo.getAllByPartie")
     * @param PartieRepository $partieRepository
     * @param VilleRepository $villeRepository
     * @param Request $request
     * @param PhotoRepository $photoRepository
     */
    public function getAllPhotos(PartieRepository $partieRepository,
                                 VilleRepository $villeRepository,
                                 Request $request,
                                 PhotoRepository $photoRepository){
        $token =  $request->get('token');
        $villeId = $partieRepository->findVilleOfPartie($token);
        $ville = $villeRepository->find($villeId[0]);
        $photos = $photoRepository->findAllPhotos($ville);
        dump($ville);
        return new JsonResponse($photos);
    }
}
