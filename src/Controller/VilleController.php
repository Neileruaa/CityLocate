<?php

namespace App\Controller;

use App\Repository\VilleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class VilleController extends AbstractController
{
    /**
     * @Route("/getAllVille", name="Ville.getAll", methods={"GET"})
     * @param VilleRepository $villeRepository
     * @return JsonResponse
     */
    public function index(VilleRepository $villeRepository)
    {
        $villes = $villeRepository->findAllNameOfVille();
        return new JsonResponse($villes);
    }

    /**
     * @Route("/ville/getLatlng", name="Ville.getLatlng")
     * @param Request $request
     * @param VilleRepository $villeRepository
     * @return JsonResponse
     */
    public function getLatLngOfCity(Request $request, VilleRepository $villeRepository){
        $villeName = $request->get('ville');
        $latLng = $villeRepository->findLatLng($villeName);
        return new JsonResponse($latLng);
    }
}
