import '../css/app.css';
import '../css/global.scss'
import 'vuetify/dist/vuetify.min.css'

import $ from 'jquery';

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify';
import App from './App'
import 'bootstrap'
import router from './router/'
import Vuex from 'vuex'
import store from './store/store'
import { Icon } from "leaflet"
// import 'leaflet/dist/leaflet.css'

Vue.use(Vuetify);
Vue.use(VueRouter);

delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

new Vue({
    el: '#app',
    router,
    template: '<App />',
    components: {App},
    store
});

