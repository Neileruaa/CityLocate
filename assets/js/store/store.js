import Vue from 'vue';
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        pseudo: '',
        difficulte: 1,
        ville: '',
        token: ''
    },
    plugins: [createPersistedState()],
    mutations: {
        CHANGE_PSEUDO: (state, pseudo) => {
            state.pseudo = pseudo
        },
        CHANGE_DIFFICULTE: (state, difficulte) => {
            state.difficulte = difficulte
        },
        CHANGE_VILLE: (state, ville) => {
            state.ville = ville
        },
        CHANGE_TOKEN: (state, token) => {
            state.token = token
        }
    },
    getters: {
        pseudo: state => state.pseudo,
        difficulte: state => state.difficulte,
        ville: state => state.ville,
        token: state => state.token
    },
    actions: {
        changePseudo : (store, pseudo) => {
            store.commit('CHANGE_PSEUDO', pseudo)
        },
        changeDifficulte: (store, difficulte) => {
            store.commit('CHANGE_DIFFICULTE', difficulte)
        },
        changeVille: (store, ville) => {
            store.commit('CHANGE_VILLE', ville)
        },
        changeToken: (store, token) => {
            store.commit('CHANGE_TOKEN', token)
        }
    }
});