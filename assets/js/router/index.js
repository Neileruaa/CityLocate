import Router from 'vue-router'

import Carousel from '../components/Carousel'
import Navbar from '../components/Navbar'
import Game from '../components/Game'
import gameStarter from '../components/gameStarter'
import gameChoice from '../components/gameChoice'
import gamePlay from '../components/gamePlay'
import beginGame from '../components/beginGame'
import gameFinal from "../components/gameFinal";

export default new Router ({
    mode: 'history',
    routes : [
        {
            path: '/',
            name: 'homepage',
            components:{
                header: Navbar,
                home: Carousel
            }
        },
        {
            path: '/vue/game',
            name: 'game',
            components:{
                header: Navbar,
                content: Game,
            },
            children: [
                {
                    path: 'start',
                    name: 'start',
                    components:{
                        gameContent: gameStarter
                    }
                },
                {
                    path: 'choice',
                    name: 'choice',
                    components:{
                        gameContent: gameChoice
                    }
                },
                {
                    path: 'play',
                    name: 'play',
                    components:{
                        gameContent: gamePlay
                    }
                },
                {
                    path: 'beginGame',
                    name: 'beginGame',
                    components:{
                        gameContent: beginGame
                    }
                },
                {
                    path: 'gameFinal',
                    name: 'gameFinal',
                    components:{
                        gameContent: gameFinal
                    }
                }
            ]
        }
    ]
})